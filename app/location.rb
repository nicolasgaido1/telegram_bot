require 'active_model'

class Location
  include ActiveModel::Validations

  attr_accessor :latitude, :longitude

  validates :latitude, :longitude, presence: true
  validates_format_of :latitude, :longitude, with: /\A[-+]?\d{1,3}\.\d+\z/
  def initialize(latitude, longitude)
    @latitude = latitude
    @longitude = longitude
  end
end
