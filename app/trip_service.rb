API_URL = 'https://router.hereapi.com'.freeze
require 'faraday'
class TripService
  def initialize(apikey = ENV['HERE_API_KEY'])
    @apikey = apikey
    @url = API_URL
    @connection = Faraday::Connection.new @url
  end

  def get_route(origin, destination, transport_mode)
    # rubocop:disable Layout/LineLength
    routes_url = "/v8/routes?apikey=#{@apikey}&destination=#{destination.latitude},#{destination.longitude}&origin=#{origin.latitude},#{origin.longitude}&return=summary&transportMode=#{transport_mode}"
    # rubocop:enable Layout/LineLength
    response = @connection.get routes_url
    JSON.parse(response.body)
  end

  def get_trip_duration(origin, destination, transport_mode)
    response = get_route(origin, destination, transport_mode)
    route = response['routes'][0]
    section = route['sections'][0]
    summary = section['summary']
    summary['duration']
  end
end
