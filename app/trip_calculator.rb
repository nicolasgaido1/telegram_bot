require "#{File.dirname(__FILE__)}/trip_service"
class TripCalculator
  include ActiveModel::Validations

  def initialize
    @trip_service = TripService.new
  end

  def get_trip_duration(origin, destination, transport_mode)
    @trip_service.get_trip_duration(origin, destination, transport_mode)
  end
end
