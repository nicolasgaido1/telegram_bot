require 'spec_helper'
require "#{File.dirname(__FILE__)}/../app/location"
require "#{File.dirname(__FILE__)}/../app/trip_service"

# rubocop:disable Metrics/MethodLength
def trip_service_api_call(apikey, origin, destination, transport_mode)
  body = {
    "routes": [
      {
        "id": '82333e8d-6773-4ef4-9080-bb1a1e0354e9',
        "sections": [
          {
            "id": '4ae39c60-e46a-4483-b7bb-7deff099de21', "type": 'vehicle',
            "departure": {
              "time": '2023-10-29T18:24:15+01:00',
              "place": {
                "type": 'place',
                "location": {
                  "lat": 52.5309838,
                  "lng": 13.3845671
                },
                "originalLocation": {
                  "lat": 52.5307999,
                  "lng": 13.3847
                }
              }
            },
            "arrival": {
              "time": '2023-10-29T18:25:59+01:00',
              "place": {
                "type": 'place',
                "location": {
                  "lat": 52.5323264,
                  "lng": 13.378874
                },
                "originalLocation": {
                  "lat": 52.5323,
                  "lng": 13.3789
                }
              }
            },
            "summary": {
              "duration": 104,
              "length": 538,
              "baseDuration": 88
            },
            "transport": {
              "mode": 'car'
            }
          }
        ]
      }
    ]
  }

  stub_request(:get, "https://router.hereapi.com/v8/routes?apikey=#{apikey}&destination=#{destination.latitude},#{destination.longitude}&origin=#{origin.latitude},#{origin.longitude}&return=summary&transportMode=#{transport_mode}")
    .with(
      headers: {
        'Accept' => '*/*',
        'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
        'User-Agent' => 'Faraday v2.7.4'
      }
    )
    .to_return(status: 200, body: body.to_json, headers: {})
end
# rubocop:enable Metrics/MethodLength

describe 'TripService' do
  let(:origin) { Location.new(52.5308, 13.3847) }
  let(:destination) { Location.new(52.5323, 13.3789) }
  let(:apikey) { 'fake_apikey' }
  let(:trip_service) { TripService.new(apikey) }

  it 'when i call the trip service then i get a response' do
    transport_mode = 'car'

    trip_service_api_call(apikey, origin, destination, transport_mode)
    trip_duration = trip_service.get_trip_duration(origin, destination, transport_mode)
    expect(trip_duration).to eq 104
  end
end
