require 'spec_helper'
require "#{File.dirname(__FILE__)}/../app/location"

describe 'Location' do
  it 'valid?' do
    location = Location.new(52.5308, 13.3847)
    expect(location).to be_valid
  end

  it 'should not be valid when passing nil attribute' do
    location = Location.new(52.5308, nil)
    expect(location).not_to be_valid
  end

  it 'should not be valid when passing attributes as string' do
    location = Location.new('hola', 'chau')
    expect(location).not_to be_valid
  end
end
